#!/usr/bin/env bash
out=`find . -name '*.java' -exec grep 'http://' /dev/null {} +`
status=$?
 
if test $status -eq 0
then
    echo "\HTTP protocol found in\ "
    echo "${out}"
else
    echo "\HTTP protocol not found"
fi

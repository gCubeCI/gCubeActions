#!/usr/bin/env bash
out=`find . -name pom.xml -exec grep 'slf4j-log4j' /dev/null {} + || echo 'empty'`
if [[ ["${out}" != "empty"] && "$(grep pom.xml <<< ${out})" ]]; then
    echo "${out}"
    echo "slf4j-log4j found"
else
    echo "slf4j-log4j not found"
fi
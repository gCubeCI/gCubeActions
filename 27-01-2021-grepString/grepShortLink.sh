#!/usr/bin/env bash
out=`find . -name '*.java' -exec grep 'http://goo.gl' /dev/null {} + || echo 'empty'`
if [[ ["${out}" != "empty"] && "$(grep '\.java' <<< ${out})" ]]; then
    echo "${out}"
    echo "short link found"
else
    echo "short link not found"
fi
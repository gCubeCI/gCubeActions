#!/usr/bin/env bash
out=`find . -name '*.java' -exec grep 'UmaJWTProvider' /dev/null {} + || echo 'empty'`
if [[ ["${out}" != "empty"] && "$(grep '\.java' <<< ${out})" ]]; then
    echo "UmaJwtToken class found in "
    echo "${out}"
else
    echo "UmaJwtToken class not found"
fi

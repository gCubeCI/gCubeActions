#!/usr/bin/env bash
out=`find . -name '*.java' -exec grep 'gfeed.d4science.org' /dev/null {} + || echo 'empty'`
if [[ ["${out}" != "empty"] && "$(grep '\.java' <<< ${out})" ]]; then
    echo "gFeed endpoint found in "
    echo "${out}"
else
    echo "gFeed endpoint not found"
fi

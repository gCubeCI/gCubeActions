#!/usr/bin/env bash
out=`find . -name '*.java' -exec grep 'http://' /dev/null {} + || echo 'empty'`
if [[ ["${out}" != "empty"] && "$(grep '\.java' <<< ${out})" ]]; then
    echo "${out}"
    echo "HTTP protocol found"
else
    echo "HTTP protocol not found"
fi

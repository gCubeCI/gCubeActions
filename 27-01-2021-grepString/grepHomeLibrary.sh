#!/usr/bin/env bash
out=`find . -name pom.xml -exec grep 'home-library' /dev/null {} + || echo 'empty'`
if [[ ["${out}" != "empty"] && "$(grep pom.xml <<< ${out})" ]]; then
    echo "${out}"
    echo "Home Library found"
else
    echo "Home Library not found"
fi
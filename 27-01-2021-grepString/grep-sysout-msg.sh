#!/usr/bin/env bash
out=`find . -name '*.java' -exec grep '****CompleteResourceProfile' /dev/null {} + || echo 'empty'`
if [[ ["${out}" != "empty"] && "$(grep '\.java' <<< ${out})" ]]; then
    echo "sysout found in "
    echo "${out}"
else
    echo "sysout not found"
fi
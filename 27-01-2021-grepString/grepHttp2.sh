#!/usr/bin/env bash
out=`find . -name '*.java' -exec grep '-http://' /dev/null {} +`
if [[ -n "${out}" ]]; then
    echo "HTTP protocol found in "
    echo "${out}"
else
    echo "HTTP protocol not found"
fi

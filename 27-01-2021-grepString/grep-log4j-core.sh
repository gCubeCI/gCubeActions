#!/usr/bin/env bash
out=`find . -name pom.xml -exec grep 'log4j-core' /dev/null {} + || echo 'empty'`
if [[ ["${out}" != "empty"] && "$(grep pom.xml <<< ${out})" ]]; then
    echo "${out}"
    echo "log4j-core found"
else
    echo "log4j-core not found"
fi

#!/usr/bin/env bash
out=`find . -name pom.xml -exec grep -E 'imagej|itext|jackrabbit-jcr-commons|jackrabbit-jcr-rmi-custom|jcr|jfile|tika-core|xstream|xmlpull|xml-apis|xpp3_min' /dev/null {} + || echo 'empty'`
if [[ ["${out}" != "empty"] && "$(grep pom.xml <<< ${out})" ]]; then
    echo "${out}"
    echo "Home Library dep found"
else
    echo "Home Library dep not found"
fi

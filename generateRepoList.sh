#!/usr/bin/env bash

#
# Generate the list of repositories for the selected organization on gitea
# Input: the gitea organization to browse
# Output: a file with one repository name (not URL) for each line
# Manuele Simi (ISTI-CNR)
#

organization=$1
echo "Getting the list of repos for ${organization}"
if [ -z "$organization" ]; then
    echo "Missing organization name."
    echo "Synopsis: generateRepoList.sh <org name>"
    echo "Example: generateRepoList.sh gCubeSystem"
    exit 1
  else
    #check if the organization exists
    http_code=$(curl -s -o /dev/null -w "%{http_code}" "https://code-repo.d4science.org/api/v1/orgs/${organization}")
    echo "Does org ${organization} exist? ${http_code}=200"
    if [ "$http_code" -ne 200 ]; then
        echo "Org ${organization} does not exist"
        exit 1
    fi
fi
pageCounter=1
timestamp=$(date +"%Y-%m-%d_%H-%M-%S")
empty="false"
echo "" > allp.json

#query all the pages until an empty result is returned
while [ $empty = "false" ]
do
  page=$(curl -X GET "https://code-repo.d4science.org/api/v1/orgs/${organization}/repos?page=${pageCounter}&limit=100000" -H "accept: application/json")
  echo "Length ${#page}"
  #break the loop when we get an empty json response ([])
  if [ ${#page} -le 2 ]; then
    empty="true"
  else
    echo $page | python3 -mjson.tool >> allp.json
    ((pageCounter++))
  fi
done

#generate a file with one repo name for each line
grep "\"name\": " allp.json > all_filtered.json
sed "s/^[ \t]*\"name\": \"//g" all_filtered.json > all_filtered1.txt
sed 's/\",//g' all_filtered1.txt > all_filtered2.txt
sort all_filtered2.txt > all_sorted.txt
mv all_sorted.txt ${organization}_all_sorted_$timestamp.txt
rm allp.json all_filtered.json all_filtered1.txt all_filtered2.txt

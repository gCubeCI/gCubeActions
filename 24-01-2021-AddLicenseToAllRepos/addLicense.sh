#!/usr/bin/env bash
if [[ ! -f LICENSE.md ]]; then
    curl https://code-repo.d4science.org/manuele.simi/gCubeActionsFiles/raw/branch/master/24-01-2021-AddLicenseToAllRepos/files/LICENSE.md -o LICENSE.md
    git add LICENSE.md
    git commit -m "Add LICENSE.md."
    echo "LICENSE.md added."
else
    echo "LICENSE.md already exists in this repo."
fi
Actions for the gCubeActions Pipeline
--

Container for Actions executed with the gCubeActionExecutor pipeline.

## Action
An Action is a script that is executed on a selected git repository.
Every command in the Action must be written assuming the root folder of the repository as working folder.

### Exit Code
An Action is successful if it returns 0 as exit code. Any code different from 0 makes the Action fails and halts the execution
of the pipeline process.

An exit code can be returned at any point of the Action by invoking the <code>exit code</code> command.
Unless exit is used, the exit code of the Action is the one returned by the last command of the Action.

## List of Repositories

Each action is executed on a list of repositories. The expected format of the file with the list is one repo name for each row.

For example:
````
repo_name1
repo_name2
repo_name3
...
````
The URL of the Gitea organization where to find these repositories is passed as parameter of the _gCubeActionExecutor_ 
pipeline.

## How to Generate the List
The script <code>generateRepoList.sh</code> can be executed any time to generate an up-to-date list 
of repositories in the _gCubeSystem_ organization. If a different organization has to be scanned, we can easily change 
the script or make it parametric.

The output of the script is a file named <code>gCubeSystem_all_sorted_$timestamp.txt</code>. 
The list in the output file is alphabetically ordered.
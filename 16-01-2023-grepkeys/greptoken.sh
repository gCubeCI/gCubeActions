#!/usr/bin/env bash
out=`find . -type f -exec grep '843339462' /dev/null {} + || echo 'empty'`
if [[ ["${out}" != "empty"] && "$(grep '\.java' <<< ${out})" ]]; then
    echo "${out}"
    echo "token found"
else
    echo "token not found"
fi
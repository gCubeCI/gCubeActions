#!/usr/bin/env bash
out=`find . -name 'symm.key' `
if [[ -n "${out}" ]]; then
    echo "old key found: "
    echo "${out}"
else
    echo "old key not found"
fi

#!/usr/bin/env bash
out=`find . -name '*.gcubekey' `
if [[ -n "${out}" ]]; then
    echo "gcubekey found: "
    echo "${out}"
else
    echo "gcubekey not found"
fi
